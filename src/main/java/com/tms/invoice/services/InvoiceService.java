package com.tms.invoice.services;

import com.tms.invoice.adaptor.InvoiceAdaptor;
import com.tms.invoice.dao.IInvoiceDAO;
import com.tms.invoice.dto.InvoiceDTO;
import com.tms.invoice.model.Invoice;
import com.tms.invoice.model.InvoiceItem;
import com.tms.item.dao.IItemDAO;
import com.tms.item.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;


/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
@Service
public class InvoiceService implements IInvoiceService {

    @Autowired
    private IInvoiceDAO invoiceDAO;

    @Autowired
    private IItemDAO itemDAO;

    public boolean saveInvoice(InvoiceDTO invoiceDTO){

        InvoiceAdaptor adaptor = new InvoiceAdaptor();
        Invoice invoice = adaptor.toModel(invoiceDTO);
        invoiceDAO.saveInvoice(invoice);

        String itemId = invoice.getSelectInvoiceItem();
        Item item = itemDAO.getItembyId(Integer.parseInt(itemId));



        InvoiceItem invoiceItem = null;

        invoiceItem = new InvoiceItem();
        invoiceItem.setItem(item);
        invoiceItem.setInvoice(invoice);
        invoiceItem.setQuantity(invoice.getInvoiceCount());
        invoiceDAO.saveInvoiceItem(invoiceItem);



        return true;




    }

}
