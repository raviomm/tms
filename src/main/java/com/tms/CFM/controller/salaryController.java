package com.tms.CFM.controller;

import com.tms.common.dto.Response;
import com.tms.employee.model.Employee;
import com.tms.reporting.services.IReportingService;
import com.tms.salary.services.ISalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 12/15/2017.
 */
@Controller
@RequestMapping ("/salary")
public class salaryController {

    @Autowired
    private ISalaryService salaryService;

    @Autowired
    private IReportingService reportingService;



    @RequestMapping (value = "/getEmployee" , method = RequestMethod.GET)
    @ResponseBody
    public Employee getEmployeeDetails(@RequestParam String employeenic)

    {
        Employee employee = salaryService.getEmployeeDetails(employeenic);

        return employee;
    }


    @RequestMapping (value = "/getEmpTrips" , method = RequestMethod.GET)
    @ResponseBody
    public Integer getEmployeeTrips(@RequestParam String employeenic, @RequestParam String year, @RequestParam String month)

    {
        Integer tripCount = null;
        tripCount = salaryService.getEmployeeTrips(employeenic);

        return tripCount;

    }

    @RequestMapping (value = "/getEmpTripsDateWise" , method = RequestMethod.GET)
    @ResponseBody
    public Integer getEmployeeTripsDateWise(@RequestParam String employeenic, @RequestParam String year,  @RequestParam String month) throws ParseException

    {
        Integer tripCount = null;
        tripCount = salaryService.getEmployeeTripsDateWise(employeenic, year, month);

        return tripCount;

    }

    @RequestMapping (value = "/calculateSalary" , method = RequestMethod.GET)
    @ResponseBody
    public Response<BigDecimal> getEmployeeSalary(@RequestParam String employeenic, @RequestParam String year, @RequestParam String month, @RequestParam Integer attendence)

    {
        Response<BigDecimal> response = new Response<>();
        BigDecimal salary = null;
        salary = salaryService.getEmployeeSalary(employeenic, year, month, attendence);
        response.setSuccess(true);
        if(salary == null){
            response.setSuccess(false);
        }
        response.setData(salary);

        return response;

    }

    @RequestMapping (value = "/generateReport" , method = RequestMethod.GET)
    @ResponseBody
    public Response<String> generateReport(@RequestParam String employeenic, @RequestParam String year, @RequestParam Integer month, @RequestParam Integer attendence)

    {
        Response<String> response = new Response<>();
        BigDecimal salary = null;
        Integer tripCount = null;
        Employee emp = null;
        String monthName = new DateFormatSymbols().getMonths()[month-1];
        tripCount = salaryService.getEmployeeTrips(employeenic);
        salary = salaryService.getEmployeeSalary(employeenic, year, String.valueOf(month), attendence);
        emp = getEmployeeDetails(employeenic);

        if(salary == null){
            response.setSuccess(false);
        }
        String base64String = reportingService.generateSalarySlip(employeenic, year, monthName, emp, tripCount, salary);
        response.setData(base64String);
        response.setSuccess(true);

        return response;

    }

}
