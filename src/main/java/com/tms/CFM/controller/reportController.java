package com.tms.CFM.controller;

import com.tms.common.dto.Response;
import com.tms.employee.dto.EmployeeDTO;
import com.tms.employee.services.IEmployeeService;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.services.IFleetService;
import com.tms.reporting.services.IReportingService;
import com.tms.route.dto.RouteDTO;
import com.tms.route.services.IRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/9/2018.
 */
@Controller
@RequestMapping("report")
public class reportController {

    @Autowired
    IReportingService reportingService;

    @Autowired
    IEmployeeService employeeService;

    @Autowired
    IFleetService fleetService;



    @RequestMapping(value = "/getAllRoutesMonthWise", method = RequestMethod.GET)
    @ResponseBody
    public Response<FleetDTO> getFleetHistoryDateWise(@RequestParam String year, @RequestParam String month) {

        Response<FleetDTO> response = new Response();
        List<FleetDTO> fleets = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            fleets = fleetService.getRouteByDate(year, month);
        }
        response.setTableData(fleets);
        response.setSuccess(true);
        return response;
    }



    @RequestMapping(value = "/getAllActiveEmployees", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllActiveEmployees() {
        Response<EmployeeDTO> response = new Response();
        List<EmployeeDTO> employees = new ArrayList<>();

        employees = employeeService.getAllActiveEmployees();
        response.setTableData(employees);
        response.setSuccess(true);
        return response;

    }

}