package com.tms.reporting.services;

import com.tms.employee.model.Employee;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 12/15/2017.
 */

@Service
public class ReportingService implements IReportingService {

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public String generateSalarySlip(String nic, String year, String month, Employee emp, Integer fleetAttendence, BigDecimal salary) {

        String base64String = "";

        try {
            Resource resource  = resourceLoader.getResource("file:C:\\Users\\RAVI KALUARACHCHI\\Desktop\\development\\tms\\src\\main\\java\\com\\tms\\reporting\\resources\\employeeReport.jrxml");

            InputStream employeeReportStream = resource.getInputStream();
            JasperReport jasperReport
                    = JasperCompileManager.compileReport(employeeReportStream);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("employeeName",emp.getFirstName());
            parameters.put("amount", salary);
            parameters.put("epf_no", emp.getEpfNo());
            parameters.put("address", emp.getEmployeeAddress());
            parameters.put("month", month);
            parameters.put("year",year);
            parameters.put("employeeNIC", nic);
            parameters.put("fleetAttend", fleetAttendence);
            parameters.put("basicSalary", emp.getEmployeeBasicsalary());
            parameters.put("allowance", emp.getEmployeeAllowance());




            JasperPrint jasperPrint
                    = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource(1) );
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(outStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            base64String = Base64.getEncoder().encodeToString(outStream.toByteArray());
        } catch (JRException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return base64String;

    }
}
