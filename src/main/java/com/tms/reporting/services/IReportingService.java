package com.tms.reporting.services;

import com.tms.employee.model.Employee;

import java.math.BigDecimal;

/**
 * Created by hp on 12/15/2017.
 */
public interface IReportingService {

    public String generateSalarySlip(String nic, String year, String month, Employee emp, Integer fleetAttendence, BigDecimal salary);
}
