package com.tms.fleet.services;


import com.tms.employee.dto.EmployeeDTO;
import com.tms.employee.model.Employee;
import com.tms.employee.services.IEmployeeService;
import com.tms.fleet.adaptor.FleetAdaptor;
import com.tms.fleet.dao.IFleetDAO;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.model.Fleet;
import com.tms.product.model.Product;
import com.tms.product.services.IProductService;
import com.tms.route.model.Route;
import com.tms.route.services.IRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */

@Service
public class FleetService implements IFleetService {


    @Autowired
    private IFleetDAO fleetDAO;

    @Autowired
    private IEmployeeService employeeServices;

    @Autowired
    private IRouteService routeService;

    @Autowired
    private IProductService productService;

    @Override
    public List<FleetDTO> getAllFleets() {
        FleetAdaptor adaptor = new FleetAdaptor();
        List<FleetDTO> dtoList = new ArrayList<>();
        List<Fleet> fleetList = fleetDAO.getAllActInaFleets();
        if(fleetList != null & !fleetList.isEmpty()){
            for(Fleet fleet : fleetList){
                dtoList.add(adaptor.toDTO(fleet));
            }
        }
        return dtoList;
    }



    @Override
    public  List<EmployeeDTO> getUsableDrivers(){

        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<EmployeeDTO> driverList = employeeServices.getAllDrivers();
        List<Fleet> activeDrivers = fleetDAO.getUsableDrivers();

        if(activeDrivers.size() >0) {
            for (EmployeeDTO driverD : driverList) {

                for (Fleet aDriver : activeDrivers) {

                    if (!driverD.getEmployeeId().equals(aDriver.getSelectDriver())) {

                        dtoList.add(driverD);
                    }
                }
            }
        }else {
            return driverList;
        }

        return dtoList;
    }


    @Override
    public  List<EmployeeDTO> getUsableHelpers(){


        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<EmployeeDTO> helperList = employeeServices.getAllHelpers();
        List<Fleet> activeHelpers = fleetDAO.getUsableHelpers();

        if(activeHelpers.size() >0) {
            for (EmployeeDTO helperH : helperList) {

                for (Fleet aHelper : activeHelpers) {

                    if (!helperH.getEmployeeId().equals(aHelper.getSelectHelper())) {

                        dtoList.add(helperH);
                    }
                }
            }
        }else {
            return helperList;
        }

        return dtoList;
    }


    @Override
    public List<FleetDTO> getFleetByDate(String year, String month) {
        Calendar c = Calendar.getInstance();   // this takes current date
        List<FleetDTO> fleetDTOList = new ArrayList<>();
        FleetAdaptor adaptor = new FleetAdaptor();

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<Fleet> fleetList = fleetDAO.getFleetsByMonth(startDate, endDate);
        if(fleetList != null && !fleetList.isEmpty()){
            for(Fleet fleet : fleetList){
                fleetDTOList.add(adaptor.toDTO(fleet));
            }
        }
        return fleetDTOList;

    }


    //get monthly routes to the Reports
    @Override
    public List<FleetDTO> getRouteByDate(String year, String month) {
        Calendar c = Calendar.getInstance();   // this takes current date
        List<FleetDTO> fleetDTOList = new ArrayList<>();
        FleetAdaptor adaptor = new FleetAdaptor();

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<Fleet> FleetList = fleetDAO.getRoutesByMonth(startDate, endDate);
        if(FleetList != null && !FleetList.isEmpty()){
            for(Fleet fleet : FleetList){
                fleetDTOList.add(adaptor.toDTO(fleet));
            }
        }
        if(fleetDTOList != null && !fleetDTOList.isEmpty()){
            for(FleetDTO fleetDTO : fleetDTOList){
                EmployeeDTO driver = employeeServices.getEmployeeByNIC(fleetDTO.getSelectDriver());
                Employee helper = employeeServices.getEmployeeByID(Integer.parseInt(fleetDTO.getSelectHelper()));
                Route route = routeService.getRouteByID(Integer.parseInt(fleetDTO.getSelectRoute()));
                Route routeId = routeService.getRouteByID(Integer.parseInt(fleetDTO.getSelectRoute()));
                Product product = productService.getProductByID(Integer.parseInt(fleetDTO.getSelectProduct()));
                fleetDTO.setSelectRoute(route.getRouteName());
                fleetDTO.setDriverName(driver.getEmployeeFirstName());
                fleetDTO.setHelperName(helper.getFirstName());
                fleetDTO.setRouteId(routeId.getRouteId());
                fleetDTO.setProductName(product.getProductName());

            }
        }

        return fleetDTOList;

    }


    @Override
    public FleetDTO getFleetByName(String fleetName) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = fleetDAO.getFleetByName(fleetName);
        if(fleet != null){
            return adaptor.toDTO(fleet);
        } else {
            return null;
        }
    }



    @Override
    public boolean saveFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Employee selectedDriver = employeeServices.getEmployeeByID(Integer.parseInt(fleetDTO.getSelectDriver()));
        Fleet fleet = adaptor.toModel(fleetDTO);
        fleet.setSelectDriver(selectedDriver);
        return fleetDAO.saveFleet(fleet);
    }


    @Override
    public boolean updateFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = adaptor.toModel(fleetDTO);
        Employee selectedDriver = employeeServices.getEmployeeByID(Integer.parseInt(fleetDTO.getSelectDriver()));
        fleet.setSelectDriver(selectedDriver);
        Fleet existingFleet = fleetDAO.getFleetById(fleet.getFleetId());
        if(existingFleet != null){

            return fleetDAO.updateFleet(fleet);
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFleet(FleetDTO fleetDTO) {
        FleetAdaptor adaptor = new FleetAdaptor();
        Fleet fleet = adaptor.toModel(fleetDTO);
        Fleet existingFleet = fleetDAO.getFleetById(fleet.getFleetId());
        if(existingFleet != null){
            return fleetDAO.deleteFleet(existingFleet);
        } else {
            return false;
        }
    }



}
