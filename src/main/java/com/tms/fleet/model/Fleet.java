package com.tms.fleet.model;

import com.tms.employee.model.Employee;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */

@Entity
@Table (name ="fleet")
public class Fleet {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "fleet_id")
    private Integer fleetId;
    @Column(name = "fleet_ref")
    private String fleetName;
    @Column (name = "select_route")
    private String selectRoute;
    @Column (name = "select_product")
    private String selectProduct;
    @Column (name = "select_vehicle")
    private String selectVehicle;
    @OneToOne
    @JoinColumn (name = "select_driver")
    private Employee selectDriver;
    @Column (name = "select_helper")
    private String selectHelper;
    @Column (name = "start_time")
    private Time fleetStartTime;
    @Column (name = "petty_cash")
    private String fleetPettyCash;
    @Column(name = "petty_cash_description")
    private String fleetPettyCashDescription;
    @Column (name = "due_date")
    private Date fleetDueDate;
    @Column (name = "fleet_status")
    private String fleetStatus;


    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getSelectRoute() {
        return selectRoute;
    }

    public void setSelectRoute(String selectRoute) {
        this.selectRoute = selectRoute;
    }

    public String getSelectProduct() {
        return selectProduct;
    }

    public void setSelectProduct(String selectProduct) {
        this.selectProduct = selectProduct;
    }

    public String getSelectVehicle() {
        return selectVehicle;
    }

    public void setSelectVehicle(String selectVehicle) {
        this.selectVehicle = selectVehicle;
    }

    public Employee getSelectDriver() {
        return selectDriver;
    }

    public void setSelectDriver(Employee selectDriver) {
        this.selectDriver = selectDriver;
    }

    public String getSelectHelper() {
        return selectHelper;
    }

    public void setSelectHelper(String selectHelper) {
        this.selectHelper = selectHelper;
    }

    public Time getFleetStartTime() {
        return fleetStartTime;
    }

    public void setFleetStartTime(Time fleetStartTime) {
        this.fleetStartTime = fleetStartTime;
    }

    public String getFleetPettyCash() {
        return fleetPettyCash;
    }

    public void setFleetPettyCash(String fleetPettyCash) {
        this.fleetPettyCash = fleetPettyCash;
    }

    public String getFleetPettyCashDescription() {
        return fleetPettyCashDescription;
    }

    public void setFleetPettyCashDescription(String fleetPettyCashDescription) {
        this.fleetPettyCashDescription = fleetPettyCashDescription;
    }

    public Date getFleetDueDate() {
        return fleetDueDate;
    }

    public void setFleetDueDate(Date fleetDueDate) {
        this.fleetDueDate = fleetDueDate;
    }

    public String getFleetStatus() {
        return fleetStatus;
    }

    public void setFleetStatus(String fleetStatus) {
        this.fleetStatus = fleetStatus;
    }
}
