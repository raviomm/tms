package com.tms.fleet.adaptor;

import com.tms.common.adaptor.BasicAdaptor;
import com.tms.fleet.dto.FleetDTO;
import com.tms.fleet.model.Fleet;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;




/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */
public class FleetAdaptor extends BasicAdaptor <Fleet,FleetDTO> {

    @Override
    public Fleet toModel (FleetDTO dto){

        Fleet fleet = new Fleet();
        fleet.setFleetId(dto.getFleetId());
        fleet.setFleetName(dto.getFleetName());
        fleet.setSelectProduct(dto.getSelectProduct());
        fleet.setSelectRoute(dto.getSelectRoute());
        fleet.setSelectVehicle(dto.getSelectVehicle());
        //fleet.setSelectDriver(dto.getSelectDriver());
        fleet.setSelectHelper(dto.getSelectHelper());
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        try {
            fleet.setFleetStartTime(new Time(formatter.parse(dto.getFleetStartTime()).getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date date1= null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getFleetDueDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        fleet.setFleetDueDate(date1);
        fleet.setFleetPettyCash(dto.getFleetPettyCash());
        fleet.setFleetPettyCashDescription(dto.getFleetPettyCashDescription());
        fleet.setFleetStatus(dto.getFleetStatus());

        return fleet;

    }

    @Override
    public FleetDTO toDTO (Fleet model){

        FleetDTO dto = new FleetDTO();
        dto.setFleetId(model.getFleetId());
        dto.setFleetName(model.getFleetName());
        dto.setSelectProduct(model.getSelectProduct());
        dto.setSelectRoute(model.getSelectRoute());
        dto.setSelectVehicle(model.getSelectVehicle());
        dto.setSelectDriver(model.getSelectDriver().getEmployeeNIC());
        dto.setSelectDriverId(model.getSelectDriver().getEmpId().toString());
        dto.setSelectHelper(model.getSelectHelper());
        dto.setFleetStartTime(model.getFleetStartTime().toString());
        dto.setFleetDueDate(model.getFleetDueDate().toString());
        dto.setFleetPettyCash(model.getFleetPettyCash());
        dto.setFleetPettyCashDescription(model.getFleetPettyCashDescription());
        dto.setFleetStatus(model.getFleetStatus());

        return dto;


    }

}
