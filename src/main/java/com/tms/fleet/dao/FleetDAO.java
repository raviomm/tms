package com.tms.fleet.dao;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tms.fleet.model.Fleet;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */

@Transactional
@Repository
public class FleetDAO implements IFleetDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Fleet> getAllFleets() {
        String hql = "FROM  Fleet as f ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public List<Fleet> getAllActInaFleets() {
        String hql = "SELECT f FROM Fleet as f WHERE f.fleetStatus = 'ACT' or f.fleetStatus = 'INA' ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).getResultList();
    }


    @Override
    public List<Fleet> getAllCompCanFleets() {
        String hql = "SELECT f FROM Fleet as f WHERE f.fleetStatus = 'CSL' or f.fleetStatus = 'CML' ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public List<Fleet> getEmployeeTrips(String empNic) {
        String hql = "SELECT f FROM Fleet as f WHERE f.selectDriver.employeeNIC = :empNic and f.fleetStatus = 'CML' ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).setParameter("empNic",empNic).getResultList();
    }

    @Override
    public List<Fleet> getEmployeeDateWise(String empNic, Date startDate, Date endDate) throws ParseException {
        String hql = "SELECT f FROM Fleet as f WHERE f.selectDriver.employeeNIC = :empNic and f.fleetStatus = 'CML' and f.fleetDueDate between  :startDate and :endDate ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).setParameter("empNic",empNic).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

    @Override
    public List<Fleet> getFleetsByMonth(Date startDate, Date endDate) {
        String hql = "SELECT f FROM Fleet as f WHERE f.fleetDueDate between  :startDate and :endDate ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

    @Override
    public List<Fleet> getRoutesByMonth(Date startDate, Date endDate) {
        String hql = "SELECT f FROM Fleet as f WHERE f.fleetDueDate between  :startDate and :endDate ORDER BY f.fleetId";
        return (List<Fleet>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }


    @Override
    public Fleet getFleetByName(String fleetNme) {
        Fleet fleet = null;
        String hql = "FROM Fleet as f WHERE f.fleetName = :fleetNa";
        List<Fleet> fleets = entityManager.createQuery(hql).setParameter("fleetNa", fleetNme).getResultList();
        if(fleets != null && fleets.size() > 0){
            fleet = fleets.get(0);
        }

        return fleet;
        
    }



    @Override
    public List <Fleet> getUsableDrivers(){
        String hq1 = "SELECT f FROM Fleet as f WHERE f.fleetStatus = 'CML' and f.fleetStatus = 'CSL'";
        return  (List<Fleet>) entityManager.createQuery(hq1).getResultList();

        }


    @Override
    public List <Fleet> getUsableHelpers(){
        String hq1 = "SELECT f FROM Fleet as f WHERE f.fleetStatus = 'CML' and f.fleetStatus = 'CSL'";
        return  (List<Fleet>) entityManager.createQuery(hq1).getResultList();

    }

    @Override
    public boolean saveFleet(Fleet fleet) {
        Fleet persistedFleet = null;
        entityManager.persist(fleet);
        persistedFleet = getFleetById(fleet.getFleetId());
        if(persistedFleet != null){
            return  true;
        }
        return false;
    }

    @Override
    public Fleet getFleetById(Integer fleetId) {
        Fleet fleet = entityManager.find(Fleet.class, fleetId);
        return fleet;
    }

    @Override
    public boolean updateFleet(Fleet fleet) {
        Fleet persistedFleet = null;
        entityManager.merge(fleet);
        persistedFleet = getFleetById(fleet.getFleetId());
        if(persistedFleet != null){
            return  true;
        }
        return false;
    }

    @Override
    public boolean deleteFleet(Fleet fleet) {
        try{
            entityManager.remove(fleet);
        }catch (Exception e){
            return false;
        }

        return true;
    }






}
