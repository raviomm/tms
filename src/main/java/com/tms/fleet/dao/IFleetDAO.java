package com.tms.fleet.dao;

import com.tms.fleet.model.Fleet;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 1/2/2018.
 */
public interface IFleetDAO {

     List<Fleet> getAllFleets();





    Fleet getFleetByName(String fleetNme);

    boolean saveFleet(Fleet fleet);

    Fleet getFleetById(Integer fleetId);

    boolean updateFleet(Fleet fleet);

    boolean deleteFleet(Fleet fleet);


     List <Fleet> getUsableDrivers();

    List <Fleet> getUsableHelpers();

    List<Fleet> getAllActInaFleets();

    List<Fleet> getAllCompCanFleets();

    List<Fleet> getEmployeeTrips(String empNic);

    List<Fleet> getEmployeeDateWise(String empNic, Date startDate, Date endDate) throws ParseException;

    List<Fleet> getFleetsByMonth(Date startDate, Date endDate);

    List<Fleet> getRoutesByMonth(Date startDate, Date endDate);

}
