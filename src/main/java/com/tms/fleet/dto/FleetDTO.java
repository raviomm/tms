package com.tms.fleet.dto;


/**
 * Created by RAVI KALUARACHCHI on 1/18/2018.
 */
public class FleetDTO {

    private Integer fleetId;
    private String fleetName;
    private String selectRoute;
    private String selectProduct;
    private String selectVehicle;
    private String selectDriver;
    private String selectDriverId;
    private String selectHelper;
    private String fleetStartTime;
    private String fleetPettyCash;
    private String fleetPettyCashDescription;
    private String fleetDueDate;
    private String fleetStatus;
    private String driverName;
    private String helperName;
    private Integer RouteId;
    private String ProductName;

    public String getProductName() { return ProductName;}

    public void setProductName(String productName) { ProductName = productName;}

    public Integer getRouteId() {return RouteId;}

    public void setRouteId(Integer RouteId) { this.RouteId = RouteId; }

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {this.fleetId = fleetId;}

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getSelectRoute() {
        return selectRoute;
    }

    public void setSelectRoute(String selectRoute) {
        this.selectRoute = selectRoute;
    }

    public String getSelectProduct() {
        return selectProduct;
    }

    public void setSelectProduct(String selectProduct) {
        this.selectProduct = selectProduct;
    }

    public String getSelectVehicle() {
        return selectVehicle;
    }

    public void setSelectVehicle(String selectVehicle) {
        this.selectVehicle = selectVehicle;
    }

    public String getSelectDriver() {
        return selectDriver;
    }

    public void setSelectDriver(String selectDriver) {
        this.selectDriver = selectDriver;
    }

    public String getSelectHelper() {
        return selectHelper;
    }

    public void setSelectHelper(String selectHelper) {
        this.selectHelper = selectHelper;
    }

    public String getFleetStartTime() {
        return fleetStartTime;
    }

    public void setFleetStartTime(String fleetStartTime) {
        this.fleetStartTime = fleetStartTime;
    }

    public String getFleetPettyCash() {
        return fleetPettyCash;
    }

    public void setFleetPettyCash(String fleetPettyCash) {
        this.fleetPettyCash = fleetPettyCash;
    }

    public String getFleetPettyCashDescription() {
        return fleetPettyCashDescription;
    }

    public void setFleetPettyCashDescription(String fleetPettyCashDescription) { this.fleetPettyCashDescription = fleetPettyCashDescription; }

    public String getFleetDueDate() {
        return fleetDueDate;
    }

    public void setFleetDueDate(String fleetDueDate) { this.fleetDueDate = fleetDueDate;}

    public String getFleetStatus() {
        return fleetStatus;
    }

    public void setFleetStatus(String fleetStatus) {
        this.fleetStatus = fleetStatus;
    }

    public String getSelectDriverId() {
        return selectDriverId;
    }

    public void setSelectDriverId(String selectDriverId) {
        this.selectDriverId = selectDriverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getHelperName() {
        return helperName;
    }

    public void setHelperName(String helperName) {
        this.helperName = helperName;
    }
}
