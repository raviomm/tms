package com.tms.salary.services;

import com.tms.employee.model.Employee;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 8/30/2018.
 */
public interface ISalaryService {

    Employee getEmployeeDetails(String employeenic);

    Integer getEmployeeTrips(String employeenic);

    BigDecimal getEmployeeSalary(String employeenic, String year, String month, Integer attendence);

    Integer getEmployeeTripsDateWise(String employeenic, String startDate, String endDate) throws ParseException;
}
