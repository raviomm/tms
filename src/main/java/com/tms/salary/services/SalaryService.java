package com.tms.salary.services;

import com.tms.employee.dao.IEmployeeDAO;
import com.tms.employee.model.Employee;
import com.tms.fleet.dao.IFleetDAO;
import com.tms.fleet.model.Fleet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 8/30/2018.
 */
@Service
public class SalaryService implements ISalaryService {

    @Autowired
    IEmployeeDAO employeeDAO;

    @Autowired
    IFleetDAO fleetDAO;

    @Override
    public Employee getEmployeeDetails(String employeenic){

        Employee employee = employeeDAO.getEmployeeSalaryDetails(employeenic);

        return employee;
        /*return inventoryDAO.getInventoryBySubtype();*/
    }



    @Override
    public Integer getEmployeeTrips(String employeenic) {
        List<Fleet> fleetList = fleetDAO.getEmployeeTrips(employeenic);
        if(fleetList != null && fleetList.size() > 0){
            return fleetList.size();
        }
        return 0;
    }



    @Override
    public Integer getEmployeeTripsDateWise(String employeenic, String year, String month) throws ParseException {
        Calendar c = Calendar.getInstance();   // this takes current date
        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();
        List<Fleet> fleetList = fleetDAO.getEmployeeDateWise(employeenic, startDate, endDate);
        if(fleetList != null && fleetList.size() > 0){
            return fleetList.size();
        }
        return 0;
    }



    @Override
    public BigDecimal getEmployeeSalary(String employeenic, String year, String month, Integer attendence) {
        BigDecimal salary = BigDecimal.ZERO;
        Calendar c = Calendar.getInstance();   // this takes current date
        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();
        Employee employee = employeeDAO.getEmployeeSalaryDetails(employeenic);
        List<Fleet> fleetList = null;

        try {
            fleetList = fleetDAO.getEmployeeDateWise(employeenic, startDate, endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(attendence ==  null){
            return null;
        }

        if(attendence < 16){
            return null;
        }

        if(fleetList.size() == 0){
            return BigDecimal.ZERO;
        }

        if(attendence < 24){
            salary = employee.getEmployeeBasicsalary().divide(new BigDecimal("2"));
        } else {
            salary = employee.getEmployeeBasicsalary();
        }

        if(employee.getEmployeeDesignation().equals("DRIVER")){
            salary = salary.add(new BigDecimal((fleetList.size() * 500)));
        }

        if(employee.getEmployeeDesignation().equals("HELPER")){
            salary = salary.add(new BigDecimal((fleetList.size() * 400)));
        }

        return salary;
    }


}
