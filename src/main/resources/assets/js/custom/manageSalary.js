/**
 * Created by RAVI KALUARACHCHI on 12/15/2017.
 */
/**
 * Created by RAVI KALUARACHCHI on 12/14/2017.
 */


$(document).ready(function () {
    console.log('ready function called');
    $('#userIdError').hide();

    //Get Trip Attendance Date Wise
    function updateTrips(){
        var selectedEmployee = document.getElementById("searchSalaryEmployee").value;
        var year = $("#datetimepicker10").find("input").val().split("/")[1];
        var month = $("#datetimepicker10").find("input").val().split("/")[0];
        var prefix = year + '-' + month + '-';
        $.ajax({
            url: "/salary/getEmpTripsDateWise?employeenic=" + selectedEmployee + "&year="+year+"&month="+month,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                console.log(response);
                $("#tripAttendence").text(response);
            },
            error: function (error) {
                $("#tripAttendence").text('0');
            }
        });

    }



    $(function () {
        $('#datetimepicker10').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        });
        $('#datetimepicker10').on('dp.change', function(n, o) {
            updateTrips();
        });
    });


    $("#calculateSalary").click(function (e) {
        var attendence = document.getElementById("attendanceCount").value;
        var employee = document.getElementById("searchSalaryEmployee").value;
        var year = $("#datetimepicker10").find("input").val().split("/")[1];
        var month = $("#datetimepicker10").find("input").val().split("/")[0];

        $.ajax({
            url: "/salary/calculateSalary?employeenic=" + employee + "&year="+year+"&month="+month+"&attendence=" + attendence,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                console.log(response);
                $("#salaryAmount").text(response.data);
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    });



    $("#generateReportId").click(function (e) {
        var attendence = document.getElementById("attendanceCount").value;
        var employee = document.getElementById("searchSalaryEmployee").value;
        var year = $("#datetimepicker10").find("input").val().split("/")[1];
        var month = $("#datetimepicker10").find("input").val().split("/")[0];

        $.ajax({
            url: "/salary/generateReport?employeenic=" + employee + "&year="+year+"&month="+month+"&attendence=" + attendence,
            // dataType: 'json',
            type: 'get',
            success: function (response) {
                console.log(response.data);
                            var fileName = "test.pdf";
                            if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
                                var byteCharacters = atob(response.data);
                                var byteNumbers = new Array(byteCharacters.length);
                                for (var i = 0; i < byteCharacters.length; i++) {
                                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                                }
                                var byteArray = new Uint8Array(byteNumbers);
                                var blob = new Blob([byteArray], {type: 'application/pdf'});
                                //var blob_iframe = document.querySelector('#blob-src-test');
                                //blob_iframe.src = blob_url;
                                window.navigator.msSaveOrOpenBlob(blob, fileName);
                            }
                            else { // much easier if not IE
                                var pdfWindow = window.open("")
                                pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(response.data)+"'></iframe>")
                            }
            },
            error: function (error) {
                console.log(error);
            }
        });
        return false;
    });


    $("#employeeIdSearch").click(function (e) {
        e.preventDefault();
        $('#userIdError').hide();
        var selectedEmployee = document.getElementById("searchSalaryEmployee").value;
        console.log('this is message', selectedEmployee);
        $.ajax({
            url: "/salary/getEmployee?employeenic=" + selectedEmployee,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                console.log(response);
                if(response.hasOwnProperty('empId')){
                    $('#userIdError').hide();
                }
                $('#attendanceCount').prop('disabled', false);
                $('#datetimepicker10 input').prop('disabled', false);
                $("#salaryEpfNo").text(response.epfNo);
                $("#salaryEmpName").text(response.firstName + response.employeeLastName);
                $("#basicSalary").text(response.employeeBasicsalary);
                $("#tripAllowance").text(response.employeeAllowance);
                $("#empDesignation").text(response.employeeDesignation);// $.each(response, function() {
                //     //selectType.append($("<option />").val(this).text(this));
                //     //selectSubtype.append($("<option />").val(this).text(this));
                // });

            },
            error: function (error) {
                console.log(error);
                $('#userIdError').show();
                $('#attendanceCount').prop('disabled', true);
                $('#datetimepicker10 input').prop('disabled', true);
            }
        });
    });
    return false;

});


