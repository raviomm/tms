/**
 * Created by RAVI KALUARACHCHI on 12/15/2017.
 */

$(document).ready(function () {

    var table = $("#reportTable").DataTable({
        "aaSorting": [],
        "scrollX": true,
        "scrollY": 530,
        "scrollCollapse": true,
        "lengthMenu": [
            [100, 400, 1000, 5000, -1],
            [100, 400, 1000, 5000, "All"]
        ],
        "retrieve": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
    });


    //year month picker for the
    $(function () {
        $('#datetimepicker11').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        });
       $('#datetimepicker11').on('dp.change', function(n, o) {
           console.log('hkjkjk')
           reportTypeOnChange();
        });
    });


});


//Reset Button for the Report Page
function reportResetButton() {
    
    var table = $("#reportTable").DataTable({
        "aaSorting": [],
        "scrollX": true,
        "scrollY": 530,
        "scrollCollapse": true,
        "lengthMenu": [
            [100, 400, 1000, 5000, -1],
            [100, 400, 1000, 5000, "All"]
        ],
        "retrieve": true
    });


}

function reportTypeOnChange() {

    var year = $("#datetimepicker11").find("input").val().split("/")[1];
    var month = $("#datetimepicker11").find("input").val().split("/")[0];

    if(year == undefined || year == null){
        year = "2018";
    }
    if(month == undefined || month == null){
        month = "11";
    }

    var selectedType = $("#reportType").val();
    $('#reportTable').DataTable().destroy();
    $('#reportTable').empty();
    $("#dateInput1").removeAttr('disabled');
    switch(selectedType) {
        case 'Monthly Routes':

            table = $('#reportTable').DataTable({
                processing: true,
                ajax: {
                    url: "/report/getAllRoutesMonthWise?year="+year+"&month="+month,
                    "type" : "get",
                    "data":function () {
                        var fleetSearchCriteria = {};
                        return fleetSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { title: "Route ID" , data: "routeId"},
                    { title: "Route Name" , data: "selectRoute"},
                    { title: "Route Start Time", data: "fleetStartTime"},
                    { title: "Route Due Date" ,data: "fleetDueDate" }
                ],
                columnDefs: [
                    {

                    }
                ],

            });
            break;

        case 'Monthly Products':
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url: "/report/getAllRoutesMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var fleetSearchCriteria = {};
                        return fleetSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { title: "product Id" , data: "selectProduct"},
                    { title: "product Name" , data: "productName"},
                    { title: "Product Transport Start Time", data: "fleetStartTime"},
                    { title: "Product Due Date" ,data: "fleetDueDate" }
                ],
                columnDefs: [
                    {

                    }
                ]

            });
            break;

        case 'Monthly Fleets':
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url: "/report/getAllRoutesMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var fleetSearchCriteria = {};
                        return fleetSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    {title: "fleet Id", data: "fleetId" },
                    {title: "Fleet Name", data: "fleetName"},
                    {title: "Route Used", data: "selectRoute"},
                    {title: "Product Used", data: "selectProduct"},
                    {title: "Vehicle Used", data: "selectVehicle"},
                    {title: "Driver Assigned", data: "driverName"},
                    {title: "Helper Assigned", data: "helperName"},
                    {title: "Start Time", data: "fleetStartTime"},
                    {title: "Petty Cash", data: "fleetPettyCash"},
                    {title: "Due Date", data: "fleetDueDate"},
                    {title: "Fleet Status", data: "fleetStatus"},

                ],
                columnDefs: [
                    {

                    }
                ]

            });
            break;

        case 'Active Employees':
            $("#dateInput1").attr('disabled','disabled');
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url: "/report/getAllActiveEmployees",
                    "type" : "GET",
                    "data":function () {
                        var employeeSearchCriteria = {};
                        return employeeSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],

                columns: [
                    {title: "Employee Id", data: "employeeId" },
                    {title: "First Name", data: "employeeFirstName"},
                    {title: "Last Name", data: "employeeLastName"},
                    {title: "NIC", data: "employeeNIC"},
                    {title: "epf", data: "employeeEPF"},
                    {title: "Address", data: "employeeAddress"},
                    {title: "Mobile", data: "employeeMobileNumber"},
                    {title: "Basic Salary", data: "employeeBasicSalary"},
                    {title: "Designation", data: "employeeDesignation"},
                    {title: "Trip Allowance", data: "employeeAllowance"}


                ],
                columnDefs: [
                    {

                    }
                ]

            });

            break;

        default:
            var table = $("#reportTable").DataTable({
                "aaSorting": [],
                "scrollX": true,
                "scrollY": 530,
                "scrollCollapse": true,
                "lengthMenu": [
                    [100, 400, 1000, 5000, -1],
                    [100, 400, 1000, 5000, "All"]
                ],
                "retrieve": true
            });
    }

}