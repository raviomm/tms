/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
/**
 * Created by RAVI KALUARACHCHI on 12/12/2017.
 */
/**
 *Created by RAVI KALUARACHCHI on 12/8/2017.
 */

var clickedRow = false;
var table;
var table2;
var table3;
var addedInvMap = [];

$(document).ready(function() {

// table for item object
    table = $('#jobItemTable').DataTable({
        columns: [
            {title: "Item Name", data: 'selectItem'},
            {title: "Quantity", data: 'itemCount'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [2],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="itemsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.itemId);
        }
    });

    // item populating to drop down
    var selectItem = $("#selectItem");

    $.ajax({
        url: "/item/getAllItems",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectItem.append($("<option />").val(this.itemId).text(this.itemName));
            });
        }
    });


    // for add button item
    $("#itemAdd").click(function () {
        const selectItem = $('#selectItem :selected').text();
        const itemCount = $('#itemCount').val();



        if (selectItem || itemCount !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.selectItem;

                }).indexOf(selectItem);

            if (index >= 0) {
                alert("Can't add same item twice");
                document.getElementById("itemAdd").disabled = true;
            }
            else {
                const updatedItem = {selectItem: selectItem,itemCount: itemCount };
                addedInvMap.push(updatedItem);
                table.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

// item object
    $('#itemAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectItem = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedInvMap.map(
            function (object, index) {

                return object.selectItem;

            }).indexOf(selectItem);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectItem').on('change', function() {
        if(this.value){
            document.getElementById("itemAdd").disabled = false;
            return;
        }
        document.getElementById("itemAdd").disabled = true;
    });






    // table for Repair object
    table2 = $('#jobRepairTable').DataTable({
        columns: [
            {title: "Repair Name", data: 'selectRepair'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [1],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="repairsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.repairId);
        }
    });

    // repair populating to drop down
    var selectRepair = $("#selectRepair");

    $.ajax({
        url: "/repair/getAllRepairs",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectRepair.append($("<option />").val(this.repairId).text(this.repairName));
            });
        }
    });

    // for add button repair
    $("#repairAdd").click(function () {
        const selectRepair = $('#selectRepair :selected').text();


        if (selectRepair !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.selectRepair;

                }).indexOf(selectRepair);

            if (index >= 0) {
                alert("Can't add same repair twice");
                document.getElementById("repairAdd").disabled = true;
            }
            else {
                const updatedItem = {selectRepair: selectRepair };
                addedInvMap.push(updatedItem);
                table2.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

    // repair object
    $('#repairAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectRepair = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedInvMap.map(
            function (object, index) {

                return object.selectRepair;

            }).indexOf(selectRepair);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table2
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectRepair').on('change', function() {
        if(this.value){
            document.getElementById("repairAdd").disabled = false;
            return;
        }
        document.getElementById("repairAdd").disabled = true;
    });

// table for rent object
    table3 = $('#jobRentTable').DataTable({
        columns: [
            {title: "Rent Code", data: 'selectRent'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [1],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="rentsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.rentId);
        }
    });

// rent populating to drop down
    var selectRent = $("#selectRent");

    $.ajax({
        url: "/rent/getAllRents",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectRent.append($("<option />").val(this.rentId).text(this.rentCode));
            });
        }
    });

    // for add button rent
    $("#rentAdd").click(function () {
        const selectRent = $('#selectRent :selected').text();


        if (selectRent !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.selectRent;

                }).indexOf(selectRent);

            if (index >= 0) {
                alert("Can't add same rent twice");
                document.getElementById("rentAdd").disabled = true;
            }
            else {
                const updatedItem = {selectRent: selectRent };
                addedInvMap.push(updatedItem);
                table3.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

// repair object
    $('#rentAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectRent = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedInvMap.map(
            function (object, index) {

                return object.selectRent;

            }).indexOf(selectRent);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table3
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectRent').on('change', function() {
        if(this.value){
            document.getElementById("rentAdd").disabled = false;
            return;
        }
        document.getElementById("rentAdd").disabled = true;
    });


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#fleetUpdateButton").hide();
    $("#fleetSaveButton").show();

    $("#searchFleet").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });

});

//function for remove button on item table column
function itemsRemove(object) {
    var selectItem = $(object).closest('tr').find('td:nth-child(1)').html();
    var itemCount = $(object).closest('tr').find('td:nth-child(2)').html();

    $("#selectItem").val(selectItem);
    $("#itemCount").val(itemCount);


    const index = addedInvMap.map(
        function (object, index) {

            return object.selectItem;

        }).indexOf(selectItem);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

//function for remove button on repair table column
function repairsRemove(object) {
    var selectRepair = $(object).closest('tr').find('td:nth-child(1)').html();


    $("#selectRepair").val(selectRepair);



    const index = addedInvMap.map(
        function (object, index) {

            return object.selectRepair;

        }).indexOf(selectRepair);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table2
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

//function for remove button on repair table column
function rentsRemove(object) {
    var selectRent = $(object).closest('tr').find('td:nth-child(1)').html();


    $("#selectRent").val(selectRent);



    const index = addedInvMap.map(
        function (object, index) {

            return object.selectRent;

        }).indexOf(selectRent);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table3
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

function fleetAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#fleetSaveButton" ).prop('disabled', false).removeClass( 'disabled' );
    $("#fleetSaveButton").show();
    $("#fleetUpdateButton").hide();

}

function resetForm() {
    $(':input', '#fleetForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

/*function disableFormData(disable) {
    $("#fleetName").prop('disabled', disable);
    $("#selectRoute").prop('disabled', disable);
    $("#selectProduct").prop('disabled', disable);
    $("#selectVehicle").prop('disabled', disable);
    $("#selectDriver").prop('disabled', disable);
    $("#selectHelper").prop('disabled', disable);
    $("#fleetStartTime").prop('disabled', disable);
    $("#fleetPettyCash").prop('disabled', disable);
    $("#fleetPettyCashDescription").prop('disabled', disable);
    $("#fleetDueDate").prop('disabled', disable);
    $("#fleetStatus").prop('disabled', disable);

}*/

/*function fillFormData(row) {
    $("#fleetId").val(row.fleetId);
    $("#fleetName").val(row.fleetName);
    $("#selectRoute").val(row.selectRoute);
    $("#selectProduct").val(row.selectProduct);
    $("#selectVehicle").val(row.selectVehicle);
    $("#selectDriver").val(row.selectDriverId);
    $("#selectHelper").val(row.selectHelper);
    $("#fleetStartTime").val(row.fleetStartTime);
    $("#fleetPettyCash").val(row.fleetPettyCash);
    $("#fleetPettyCashDescription").val(row.fleetPettyCashDescription);
    $("#fleetDueDate").val(row.fleetDueDate.substring(0,10));
    $("#fleetStatus").val(row.fleetStatus);

}*/


/*function disableUpdate(disable) {
    $("#fleetEditButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#fleetDeleteButton").prop('disabled', disable);

}*/

/*function fleetClear(disable) {
    $("#fleetClearButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#fleetEditButton" ).prop('disabled', false).removeClass( 'disabled' );
    $( "#fleetSaveButton" ).prop('disabled', true).addClass( 'disabled' );
    $("#fleetUpdateButton").hide();
    $("#fleetSaveButton").show();

}*/


/*function fleetSave() {

    var fleetDTO = {};
    fleetDTO.fleetId = $("#fleetId").val();
    fleetDTO.fleetName = $("#fleetName").val();
    fleetDTO.selectRoute = $("#selectRoute").val();
    fleetDTO.selectProduct = $("#selectProduct").val();
    fleetDTO.selectVehicle = $("#selectVehicle").val();
    fleetDTO.selectDriver = $("#selectDriver").val();
    fleetDTO.selectHelper = $("#selectHelper").val();
    fleetDTO.fleetStartTime = $("#fleetStartTime").val();
    fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
    fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
    fleetDTO.fleetDueDate = $("#fleetDueDate").val();
    fleetDTO.fleetStatus = $("#fleetStatus").val();


    $.ajax({
        url: "/fleet/saveFleetRecord", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Confirm Saving Fleet ");
                fleetClear(true);
            } else {
                alert("Error Saving Fleet  !!!");
            }
        },
        error : function (error) {
            console.log(error);
        }
    });

}*/




/*$( "#fleetUpdateButton" ).click(function(e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
    });

    swalWithBootstrapButtons({
        title: 'Confirmation',
        text: "Are you sure you want Update it?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Update',
        cancelButtonText: 'Cancel'
    }).then(function (result) {
            if (result.value) {

                var fleetDTO = {};
                fleetDTO.fleetId = $("#fleetId").val();
                fleetDTO.fleetName = $("#fleetName").val();
                fleetDTO.selectRoute = $("#selectRoute").val();
                fleetDTO.selectProduct = $("#selectProduct").val();
                fleetDTO.selectVehicle = $("#selectVehicle").val();
                fleetDTO.selectDriver = $("#selectDriver").val();
                fleetDTO.selectHelper = $("#selectHelper").val();
                fleetDTO.fleetStartTime = $("#fleetStartTime").val();
                fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
                fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
                fleetDTO.fleetDueDate = $("#fleetDueDate").val();
                fleetDTO.fleetStatus = $("#fleetStatus").val();

                $.ajax({
                    url : '/fleet/updateFleet', // or whatever
                    dataType : 'json',
                    type: 'post',
                    data: fleetDTO,
                    success : function (response) {
                        if(response.success){
                            swal({
                                type: 'success',
                                title: 'Updated',
                                text: 'Row is successfully updated'
                            })
                            fleetClear(true);
                        } else {
                            swal(
                                'Error',
                                'There is an issue in updating',
                                'error'
                            )
                        }
                    }
                })
                ;
            }
        }
    );
});*/



/*function fleetEdit(){
    if(clickedRow){
        disableFormData(true);
        $("#fleetName").prop('disabled', false);
        $("#selectProduct").prop('disabled', false);
        $("#selectRoute").prop('disabled', false);
        $( "#fleetEditButton" ).prop('disabled', true).addClass( 'disabled' );
        $( "#fleetUpdateButton" ).prop('disabled', false).removeClass( 'disabled' );
        $( "#fleetUpdateButton" ).show();
        $( "#fleetSaveButton" ).hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}*/

/*function fleetDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var fleetDTO = {};
                    fleetDTO.fleetId = $("#fleetId").val();
                    fleetDTO.fleetName = $("#fleetName").val();
                    fleetDTO.selectRoute = $("#selectRoute").val();
                    fleetDTO.selectProduct = $("#selectProduct").val();
                    fleetDTO.selectVehicle = $("#selectVehicle").val();
                    fleetDTO.selectDriver = $("#selectDriver").val();
                    fleetDTO.selectHelper = $("#selectHelper").val();
                    fleetDTO.fleetStartTime = $("#fleetStartTime").val();
                    fleetDTO.fleetPettyCash = $("#fleetPettyCash").val();
                    fleetDTO.fleetPettyCashDescription = $("#fleetPettyCashDescription").val();
                    fleetDTO.fleetDueDate = $("#fleetDueDate").val();
                    fleetDTO.fleetStatus = $("#fleetStatus").val();

                    $.ajax({
                        url: '/fleet/deleteFleet', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: fleetDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Row is successfully deleted'
                                })
                                fleetClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    });
                }
            }
        );

    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }
}*/

/*
 function searchFleet() {
 table.ajax.reload();
 }*/
