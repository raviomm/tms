/**
 * Created by RAVI KALUARACHCHI on 6/11/2018.
 */

/**
 * Created by RAVI KALUARACHCHI on 11/06/2018.
 */
var table;
var clickedRow = false;
var year;
var month;

$(document).ready(function() {
    if(year == undefined || year == null){
        year = "2018";
    }
    if(month == undefined || month == null){
        month = "11";
    }

    table = $('#fleetHistoryTable').DataTable({
        processing: true,
        ajax: {
            url: "/fleetHistory/getFleetHistoryDataWise",
            "type": "POST",
            "data":function () {
                var fleetHistorySearch = {};
                fleetHistorySearch.year = year;
                fleetHistorySearch.month = month;
                return fleetHistorySearch;
            },
            dataSrc: function(json) {

                return json.tableData;
            }
        },
        dom: 'Bfrtip',
        buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        columns: [
            {
                title: "fleet Id",
                data: "fleetId"
            },
            {
                title: "Fleet Name",
                data: "fleetName"
            },
            {
                title: "Route Used",
                data: "selectRoute"
            },
            {
                title: "Product Used",
                data: "selectProduct"
            },
            {
                title: "Vehicle Used",
                data: "selectVehicle"
            },
            {
                title: "Driver Assigned",
                data: "selectDriver"
            },
            {
                title: "Helper Assigned",
                data: "selectHelper"
            },
            {
                title: "Start Time",
                data: "fleetStartTime"
            },
            {
                title: "Petty Cash",
                data: "fleetPettyCash"
            },
            {
                title: "Petty Description",
                data: "fleetPettyCashDescription"
            },
            {
                title: "Due Date",
                data: "fleetDueDate"
            },
            {
                title: "Fleet Status",
                data: "fleetStatus"

            }

        ],
        columnDefs: [{
            targets: [2],
            visible: false,
            searchable: false
        },
            {
                targets: [8],
                visible: false,
                searchable: false
            },
            {
                targets: [11],
                visible: true,
                searchable: false
            }

        ],
    //Table Row Color code
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            switch(aData.fleetStatus){
                case 'ACT':
                    $(nRow).css('color', 'green');
                    break;
                case 'INA':
                    $(nRow).css('color', 'red');
                    break;

            }
        }

    });

    $(function () {
        $('#datetimepickerHistory').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"

            },
        });

        $('#datetimepickerHistory').on('dp.change', function(n, o) {
            updateFleets();
        });

    });




    // Disabling the dateInput field
    function dateInputDisable(disable) {

        $( "#dateInput" ).prop('disabled', true).addClass( 'disabled' );
        table.ajax.reload();

    }


    // table click event
    $('#homeTable tbody').on('click', 'tr', function() {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }


    });




});

function updateFleets() {
    year = $("#datetimepickerHistory").find("input").val().split("/")[1];
    month = $("#datetimepickerHistory").find("input").val().split("/")[0];
    if(year == undefined || year == null){
        year = "";
    }
    if(month == undefined || month == null){
        month = "";
    }
    table.ajax.reload();
}


