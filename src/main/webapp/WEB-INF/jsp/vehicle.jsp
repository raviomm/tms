<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />


	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
	
	
    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">

    <script type="text/javascript" charset="UTF-8"  src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8"  src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8"  src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>

</head>
<body>
	<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
		<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Sithumina Transport
                </a>
            </div>

            <ul class="nav">
                <li class="deactive">
                    <a href="manageHome.html">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="">
                    <a href="manageEmployee.html">
                        <i class="pe-7s-user"></i>
                        <p>Employee</p>
                    </a>
                </li>
                <li><li class="active">
                    <a href="manageVehicle.html">
                        <i class="pe-7s-note2"></i>
                        <p>Vehicle</p>
                    </a>
                </li></li>
                <li>
                    <a href="manageRoute.html">
                        <i class="pe-7s-news-paper"></i>
                        <p>Route</p>
                    </a>
                </li>
                <li>
                    <a href="manageProduct.html">
                        <i class="pe-7s-science"></i>
                        <p>Product</p>
                    </a>
                </li>
                <li>
                    <a href="manageFleet.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Fleets</p>
                    </a>
                </li>
                <li>
                    <a href="manageFleetHistory.html">
                        <i class="pe-7s-graph"></i>
                        <p>Fleet History</p>
                    </a>
                </li>
                <li>
                    <a href="manageSalary.html">
                        <i class="pe-7s-note2"></i>
                        <p>Salary Calculation</p>
                    </a>
                </li>
				<li>
                    <a href="manageReport.html">
                        <i class="pe-7s-note2"></i>
                        <p>Report Generator</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

<div class="wrapper">
	<div class="main-panel">

         <div class="content"> 
              <div class="container-fluid"> 
                <div class="row"> 
                    <div class="col-md-4">
                         <div class="card">  
                            <div class="header">
                                <h4 class="title">Vehicle Information Management</h4><br>
                            </div>
                            <div class="content">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <form id="vehicleForm" data-toggle="validator" role="form">
                                                <input id="id" type="text" hidden>
                                                    <div class="form-group">
                                                        <label for="vehcleRegNo" class="control-label required"><b>Reg No</b></label>
                                                        <input id="vehcleRegNo" type="text" class="form-control"
                                                               placeholder="Enter Reg No" value="" data-error="Enter Reg Number" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                <div class="form-group">
                                                    <label for="vehicleType"
                                                           class="control-label required"><b>Select Vehicle Type</b></label>
                                                    <select id="vehicleType"
                                                            data-error="Select Vehicle Type"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Type</option>
                                                        <option value="TRUCK">Truck</option>
                                                        <option value="LORRY">Lorry</option>
                                                        <option value="VAN">Van</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="vehicleCapacity"
                                                           class="control-label required"><b>Select Vehicle Capacity</b></label>
                                                    <select id="vehicleCapacity"
                                                            data-error="Vehicle Capacity"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Capacity</option>
                                                        <option value="250">250kg</option>
                                                        <option value="500">500kg</option>
                                                        <option value="1000">1000kg</option>
                                                        <option value="2000">2000kg</option>

                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="vehicleFuelType"
                                                           class="control-label"><b>Select Fuel Type</b></label>
                                                    <select id="vehicleFuelType"
                                                            data-error="Select Fuel Type"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Fuel Type</option>
                                                        <option value="P">Petrol</option>
                                                        <option value="D">Diesel</option>
                                                        <option value="H">Hybride</option>
                                                        <option value="E">Electric</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="vehicleUser"
                                                           class="control-label required"><b>Owner</b></label>
                                                    <select id="vehicleUser"
                                                            data-error="Select Owner Type"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select a Owner</option>
                                                        <option value="COMPANY">COMPANY</option>
                                                        <option value="Kularathna">Kularathna</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="vehicleChassisNo" class="control-label"><b>Chassis No</b></label>
                                                    <input id="vehicleChassisNo" type="text" class="form-control"
                                                           placeholder="Enter Chassis No" value="" data-error="Enter Chassis Number" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="vehicleEngineNo" class="control-label"><b>Engine No</b></label>
                                                    <input id="vehicleEngineNo" type="text" class="form-control"
                                                           placeholder="Enter Engine No" value="" data-error="Enter Engine Number" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="vehicleModel" class="control-label"><b>Model</b></label>
                                                    <input id="vehicleModel" type="text" class="form-control"
                                                           placeholder="Enter Model" value="" data-error="Enter Model" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="vehicleMilage" class="control-label required"><b>Milage</b></label>
                                                    <input id="vehicleMilage" type="text" class="form-control"
                                                           placeholder="Enter Milage" value="" data-error="Enter Milage" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="status"
                                                           class="control-label required"><b>Status</b></label>
                                                    <select id="status"
                                                            data-error="Select a Status"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select a Status</option>
                                                        <option value="ACT">Active</option>
                                                        <option value="INA">Inactive</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>

												<br>
                                                    <br>
													<button type="submit" id="vehicleSave" class="btn btn-info btn-fill pull-left">Save</button>
                                                    <button type="submit" id="vehicleUpdate" class="btn btn-info btn-fill pull-left">Update</button>
													<br>
													<br>
													<br>
													
                                    

												

												<br> <br>
												
												
											  

												
                                            </form>
                                        </div>
									</div>

									
								
                                </form>
							</div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                    <div class="pull-center">
                                        <h5>Search Vehicle</h5>

                                        <input type="text" id="serchVehicleRegNo" class="form-control1" placeholder="TYPE YOUR REG NO HERE" name="search"><br>
                                        <button type="submit" onclick="searchVehicle()" class="btn btn-info btn-fill pull-center">Search</button>&nbsp &nbsp &nbsp

                                        <br>
                                    </div><br><br>

                                    <div class="content table-responsive table-full-width">
                                        <table id="vehicleTable" class="display" width="100%"></table>

                                        <br>
                                        <br>
                                        <div class="pull-center">
                                            <button type="submit" id="vehicleAddButton" onclick="vehicleAdd()" class="btn btn-info btn-fill pull-center">Add</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleUpdateButton" onclick="vehicleUpdate()" class="btn btn-info btn-fill pull-center">Edit</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleDeleteButton" onclick="vehicleDelete()" class="btn btn-info btn-fill pull-center">Delete</button>&nbsp &nbsp &nbsp
                                            <button type="submit" id="vehicleClearButton" onclick="vehicleClear()" class="btn btn-info btn-fill pull-center">Clear</button>
                                            <br>
                                        </div>

                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
                </div>
            </div>
        </div>                  
                                    
                 
                    

                


        <footer class="footer">

        </footer>

    </div>
</div>

									


</body>

    <!-- Alert Box -->
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/js/sweetalert2.min.js"></script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/validator.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <script src="assets/js/custom/manageVehicle.js"></script>


	

</html>
