<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">

    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>
</head>
<body style="">
<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Sithumina Transport
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="active">
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li></li>
            <li class="">
                <a href="manageVehicle.html">
                    <i class="pe-7s-note2"></i>
                    <p>Vehicle</p>
                </a>
            </li>
            <li>
                <a href="manageRoute.html">
                    <i class="pe-7s-news-paper"></i>
                    <p>Route</p>
                </a>
            </li>
            <li>
                <a href="manageProduct.html">
                    <i class="pe-7s-science"></i>
                    <p>Product</p>
                </a>
            </li>
            <li>
                <a href="manageFleet.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Fleets</p>
                </a>
            </li>
            <li>
                <a href="manageFleetHistory.html">
                    <i class="pe-7s-graph"></i>
                    <p>Fleet History</p>
                </a>
            </li>
            <li>
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div>
</div>

<div class="wrapper">
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Employee Information Management</h4>
                            </div>

                            <div class="content">

                                <div class="row">
                                    <div class="col-md-4">
                                        <form id="employeeForm" data-toggle="validator" role="form">
                                            <input id="employeeId" type="text" hidden>
                                            <div class="form-group">
                                                <label for="employeeFirstName" class="control-label required"><b>First
                                                    Name</b></label>
                                                <input id="employeeFirstName" type="text" class="form-control"
                                                       placeholder="Enter First Name" value=""
                                                       data-error="Enter First Name" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeLastName" class="control-label required"><b>Last Name</b></label>
                                                <input id="employeeLastName" type="text" class="form-control"
                                                       placeholder="Enter Last Name" value=""
                                                       data-error="Enter Last Name" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeNIC" class="control-label required"><b>NIC</b></label>
                                                <input id="employeeNIC" type="text" class="form-control"
                                                       placeholder="Enter NIC" value="" data-error="Enter NIC" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeEPF" class="control-label required"><b>EPF Number</b></label>
                                                <input id="employeeEPF" type="text" class="form-control"
                                                       placeholder="Enter EPF Number" value=""
                                                       data-error="Enter EPF Number" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeAddress" class="control-label required"><b>
                                                    Address</b></label>
                                                <input id="employeeAddress" type="text" class="form-control"
                                                       placeholder="Enter Address" value="" data-error="Enter Address"
                                                       required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeMobileNumber" class="control-label"><b>Mobile
                                                    Number</b></label>
                                                <input id="employeeMobileNumber" type="text" class="form-control"
                                                       placeholder="Enter Mobile Number" value=""
                                                       data-error="nter Mobile Number" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeEmail" class="control-label"><b>Email</b></label>
                                                <input id="employeeEmail" type="text" class="form-control"
                                                       placeholder="Enter Email" value="" data-error="Enter Email"
                                                       required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeBasicSalary" class="control-label required"><b>Basic
                                                    Salary</b></label>
                                                <input id="employeeBasicSalary" type="text" class="form-control"
                                                       placeholder="Enter Basic Salary" value=""
                                                       data-error="Enter Basic Salary" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeAllowance"
                                                       class="control-label required"><b>Trip Allowance</b></label>
                                                <input id="employeeAllowance" type="text" class="form-control"
                                                       placeholder="Enter Allowance" value=""
                                                       data-error="Enter Allowance" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeDesignation"
                                                       class="control-label required"><b>Designation</b></label>
                                                <select id="employeeDesignation"
                                                        data-error="Select a designation"
                                                        class="form-control btn btn-default dropdown-toggle" required>
                                                    <option value="">Select a Designation</option>
                                                    <option value="MANAGER">Manager</option>
                                                    <option value="SUPERVISOR">Supervisor</option>
                                                    <option value="DRIVER">Driver</option>
                                                    <option value="HELPER">Helper</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="employeeStatus"
                                                       class="control-label required"><b>Status</b></label>
                                                <select id="employeeStatus"
                                                        data-error="Select a status"
                                                        class="form-control btn btn-default dropdown-toggle" required>
                                                    <option value="">Select Option</option>
                                                    <option value="ACT">Active</option>
                                                    <option value="INA">Inactive</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <button type="submit" id="employeeSave"  class="btn btn-info btn-fill pull-left"> Save </button>
                                            <button type="submit" id="employeeUpdateButton1"  class="btn btn-info btn-fill pull-left"> Update </button>
                                            <br>

                                            <br>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">

                                <%--<h5>Search Employees</h5>
                                <input type="text" id="serchEmployeeNIC" class="form-control1"
                                       placeholder="Type Employee NIC Here" name="search"><br>
                                <button type="submit" onclick="searchEmployee()"
                                        class="btn btn-info btn-fill pull-center">Search
                                </button>--%>
                                    <div class="form-group">
                                        <label for="serchEmployeeNIC" class="control-label required"><b>Select Employee</b></label>
                                        <input id="serchEmployeeNIC" type="text" class="form-control"
                                               placeholder="TYPE Employee NIC" value=""
                                               data-error="TYPE Employee NIC" required/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="alert alert-danger" role="alert" id="userIdError">
                                        <p>Enter valid user ID</p>
                                    </div>

                                    <button id="searchEmployee" onclick="searchEmployee()"
                                            class="btn btn-info btn-fill pull-left">Select
                                    </button>
                                    <br>


                                <br><br>

                                <div class="content table-responsive table-full-width">
                                    <table id="employeeTable" class="display" width="100%"></table>

                                    <br>
                                    <br>
                                    <div class="pull-center">
                                        <button type="submit" id="employeeAddButton" onclick="employeeAdd()"
                                                class="btn btn-info btn-fill pull-center">Add
                                        </button>
                                        &nbsp &nbsp &nbsp
                                        <button type="submit" id="employeeUpdateButton" onclick="employeeUpdate()"
                                                class="btn btn-info btn-fill pull-center">Edit
                                        </button>
                                        &nbsp &nbsp &nbsp
                                        <button type="submit" id="employeeDeleteButton" onclick="employeeDelete()"
                                                class="btn btn-info btn-fill pull-center">Delete
                                        </button>
                                        &nbsp &nbsp &nbsp
                                        <button type="submit" id="employeeClearButton" onclick="employeeClear()"
                                                class="btn btn-info btn-fill pull-center">Clear
                                        </button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <footer class="footer">

            </footer>

        </div>
    </div>


    <!-- Alert Box -->
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/js/sweetalert2.min.js"></script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/validator.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <!-- JQuery data table adding -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <!-- Include the js file to the page -->
    <script src="assets/js/custom/manageEmployee.js"></script>
</div>

</body>
</html>