<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <title>Spring Security Example</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" type="text/javascript"></script>


</head>
<body background="" >

</br>
</br>
<h3 style="font-size:30px;"  align="center">Welcome to Sithumina Enterprises and Transport Service</h3></br></br>
<div class="container-fluid"    align="center"  style="background-color: grey; width: 50%; margin:auto;" >

    <label id="loginError" style="display: none;"><h4 style="color: azure; font-size: 2em;" >Invalid User Name or Password</h4></label>
    <c:if test="${param.error ne null}">
        <%--<div style="color: white; font-size: 30px">Invalid credentials.</div>--%>
        <script>
            document.getElementById("loginError").style.display = 'block';

        </script>
    </c:if>

    <form action="/login" method="post" data-toggle="validator" role="form">
        <div align="center" class="card" style="background-color: deepskyblue;width: 90%;"  >
            <br>

            <label for="username" class="control-label required"><h4>UserName:</h4>
                <input type="text"  class="form-control2" placeholder="Enter Username" id="username" name="username"  maxlength="30" size="30" value="" required>
            </label>
        <br><br>

            <label for="pwd"><h4>Password:</h4>
                <input id="pwd" name="password" type="password" class="form-control2" placeholder="Enter password"  name="password"   maxlength="30" size="30" value=""
                       data-error="Enter correct password" required>
            </label>
            <br>


            <br>
            <button type="submit" class="btn btn-info btn-fill pull-left" align="center" style="width: 20%;">Login</button>
            <br>
        </div>


        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>

</div>


</body>
</html>